#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import math
import pygame
from datetime import timedelta
from PySide import QtGui
from PySide.QtCore import Qt, QTime, QTimer

pygame.init()

class CountdownWidget(QtGui.QLabel):

    def __init__(self, countdown, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self.countdown = countdown
        self.timer = QTimer(self)
        self.timer.timeout.connect(self._update_time)

    def setTime(self, countdown):
        self.countdown = countdown
        self.setText(self.countdown.toString(Qt.ISODate))

    def start(self):
        self.timer.start(1000)

    def stop(self):
        self.timer.stop()

    def _update_time(self):
        self.countdown = self.countdown.addSecs(-1)
        
        if self.countdown.second() == 0 and (self.countdown.hour() > 0 or self.countdown.minute() > 0):
            pygame.mixer.music.load("data/ding.wav")
            pygame.mixer.music.play()
        if self.countdown.second() == 15 and self.countdown.hour() == 0 and self.countdown.minute() == 0:
            pygame.mixer.music.load("data/pour.wav")
            pygame.mixer.music.play()
        if self.countdown <= QTime(0, 0, 0):
            self.timer.stop()
        self.setText(self.countdown.toString(Qt.ISODate))


class DevTimer(QtGui.QWidget):
    
    def __init__(self):
        super(DevTimer, self).__init__()
        self.data = {
            ' - select film -': []
        }
        self.notes = {'':''}
        with open('data/caffenol.txt') as f:
            c = f.readlines()

        for line in c:
            if len(line)>5:
                tmp=line.strip().split("\t")
                self.data[tmp[0] + ' @ ' + tmp[3]] = tmp

        with open('data/notes.txt') as f:
            c = f.readlines()

        for line in c:
            if len(line)>5:
                tmp=line.strip().split("\t")
                self.notes[tmp[0]] = tmp[1]
        self.initUI()

        
    def start(self):
        try:
            mf = float(self.minutes.text())
        except:
            self.minutes.setText('15')
            mf = 15.0
            
        td = timedelta(minutes=int(mf))
        
        hours, minutes = td.seconds//3600, (td.seconds//60)%60
        seconds = int(math.modf(mf)[0] * 60)
        self.countdown.setTime(QTime(hours,minutes,seconds))

        self.countdown.start()

    def stop(self):
        self.countdown.setText('')
        self.countdown.stop()
        
    def _get_notes(self, notes_ids):
        notes_ids = notes_ids.replace('[','').split(']')
        notes = ''
        for nid in notes_ids:
            if nid:
                notes += self.notes[nid] + '; '
        return notes
    
    def _get_avail_time(self, film, size):
        sizes = {'35': 4, '120': 5, 'sheet': 6}
        fid = sizes[size]
        if self.data[film][fid]:
            return self.data[film][fid]
        else:
            return max([self.data[film][4], self.data[film][5], self.data[film][6]])
                
    def _on_film_changed(self, ind):
        t = self.films.currentText()
        if t != ' - select film -':
            if len(self.data[t]) == 9:
                notes = self._get_notes(self.data[t][8])
            else:
                notes = ''
            self.devel.setText('<b>%s</b> %s' % (self.data[t][1], notes))
            minutes = self._get_avail_time(t, self.formats.currentText())
            self.minutes.setText(minutes)
        
    def initUI(self):
        
        self.setWindowIcon(QtGui.QIcon('dev.png'))
        
        grid = QtGui.QGridLayout()        
        
        self.devel = QtGui.QLabel()
        self.devel.setWordWrap(True)
        self.devel.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        
        btn = QtGui.QPushButton('Start')
        btn.clicked.connect(self.start)

        btnstop = QtGui.QPushButton('Stop')
        btnstop.clicked.connect(self.stop)
        
        self.countdown = CountdownWidget(QTime(0, 15, 0))
        self.countdown.setFont(QtGui.QFont('SansSerif', 24))
        self.countdown.setAlignment(Qt.AlignCenter)

        self.formats = QtGui.QComboBox()
        self.formats.addItems(['35','120','sheet'])
        self.formats.activated.connect(self._on_film_changed)
        
        self.films = QtGui.QComboBox()
        films_list = self.data.keys()
        films_list.sort()
        self.films.addItems(films_list)
        
        self.films.activated.connect(self._on_film_changed)
        
        self.minutes = QtGui.QLineEdit()
        self.minutes.setText('15')
        
        self.setGeometry(300, 300, 450, 250)
        self.setWindowTitle('Film Development Timer')    
        self.setFixedSize(450,170)
        
        grid.addWidget(self.devel,0,0,0,3)
        grid.addWidget(self.countdown,1,0)
                       
        grid.addWidget(self.films,2,0)
        grid.addWidget(self.formats,2,1)
        grid.addWidget(self.minutes,2,2)
        grid.addWidget(btn,3,0)
        grid.addWidget(btnstop,3,2)
        
        self.setLayout(grid)
        self.show()
        
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = DevTimer()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
